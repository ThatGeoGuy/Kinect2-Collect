﻿/*
 * Copyright (c) 2015, Jeremy Steward
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;

namespace Kinect2_Collect
{
    internal static class FrameDataWriter
    {
        /// <summary>
        ///     Writes a BitmapSource object to a PNG file located at the location specified by
        ///     filepath
        /// </summary>
        /// <param name="frame">The Bitmap object to write to PNG</param>
        /// <param name="filepath">The location to write the file to</param>
        private static void WriteImageSourceToPng(BitmapSource frame, string filepath)
        {
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(frame));

            using (var fs = new FileStream(filepath, FileMode.Create))
                encoder.Save(fs);
        }

        /// <summary>
        ///     Writes ColorFrame data (as a byte array) to a file.
        /// </summary>
        /// <param name="frame">Byte array containing the data from a ColorFrame object</param>
        /// <param name="fdesc">Frame Description for a ColorFrame object</param>
        /// <param name="filepath">Path to write the file</param>
        public static void WriteColorFrameDataToFile(byte[] frame, FrameDescription fdesc, string filepath)
        {
            var width = fdesc.Width;
            var height = fdesc.Height;
            var stride = width*((int) fdesc.BytesPerPixel);
            var bitmap = BitmapSource.Create(width, height, 96, 96, PixelFormats.Bgra32, null, frame, stride);
            WriteImageSourceToPng(bitmap, filepath);
        }

        /// <summary>
        ///     Writes the depth frame data to a text file.
        /// </summary>
        /// <param name="frame">Depth data represented as a 1D ushort array</param>
        /// <param name="filepath">The path of the file to save the data to.</param>
        public static void WriteDepthFrameDataToFile(ushort[] frame, string filepath)
        {
            var XYZs = CameraCorrections.XyzFromDepthFrame(
                frame,
                new CameraParameters(512, 424, 364.7531, 364.7531, 0, 0, 0, 0, 0, 0, 0, 0));
            using (var fs = new FileStream(filepath, FileMode.Create))
            {
                using (var bw = new BinaryWriter(fs))
                {
                    foreach (var xyz in XYZs)
                    {
                        bw.Write(xyz.Item1);
                        bw.Write(xyz.Item2);
                        bw.Write(xyz.Item3);
                    }
                }
            }
        }

        /// <summary>
        ///     Writes the InfraredFrame data to a file.
        /// </summary>
        /// <param name="frame">InfraredFrame data represented as a byte array</param>
        /// <param name="fdesc">FrameDescription object from the InfraredFrame</param>
        /// <param name="filepath">File path to write the data to</param>
        public static void WriteInfraredFrameDataToFile(byte[] frame, FrameDescription fdesc, string filepath)
        {
            var width = fdesc.Width;
            var height = fdesc.Height;
            var stride = width*((int) fdesc.BytesPerPixel);
            var bitmap = BitmapSource.Create(width, height, 96, 96, PixelFormats.Gray16, null, frame, stride);
            WriteImageSourceToPng(bitmap, filepath);
        }

        /// <summary>
        ///     Writes the BodyIndexFrame data to a file.
        /// </summary>
        /// <param name="frame">BodyIndexFrame data represented as a byte array</param>
        /// <param name="fdesc">FrameDescription object from the BodyIndexFrame</param>
        /// <param name="filepath">File path to write the data to</param>
        public static void WriteBodyIndexFrameDataToFile(byte[] frame, FrameDescription fdesc, string filepath)
        {
            var width = fdesc.Width;
            var height = fdesc.Height;
            var stride = width*((int) fdesc.BytesPerPixel);
            var bitmap = BitmapSource.Create(width, height, 96, 96, PixelFormats.Gray8, null, frame, stride);
            WriteImageSourceToPng(bitmap, filepath);
        }
    }
}
