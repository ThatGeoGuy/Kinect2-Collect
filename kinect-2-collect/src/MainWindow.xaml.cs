﻿/*
 * Copyright (c) 2015, Jeremy Steward
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using Microsoft.Kinect;

namespace Kinect2_Collect
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly object _block = new object();
        private readonly object _clock = new object();
        private readonly object _dlock = new object();
        private readonly object _ilock = new object();

        /// <summary>
        ///     Constructs and initializes the MainWindow object
        /// </summary>
        public MainWindow()
        {
            DefaultKinectSensor = null;
            FrameReader = null;
            DefaultKinectSensor = KinectSensor.GetDefault();

            if (DefaultKinectSensor != null)
            {
                DefaultKinectSensor.Open();
                if (DefaultKinectSensor.IsOpen)
                {
                    FrameReader = DefaultKinectSensor.OpenMultiSourceFrameReader(
                        FrameSourceTypes.Color |
                        FrameSourceTypes.Depth |
                        FrameSourceTypes.Infrared |
                        FrameSourceTypes.BodyIndex);
                }
            }
            FrameNumber = 0;
            IsSaveSet = false;
            PcQueue = new ProdConsQueue(8);

            ColorQueue = new Queue<Tuple<byte[], FrameDescription, string>>();
            DepthQueue = new Queue<Tuple<ushort[], string>>();
            InfraredQueue = new Queue<Tuple<byte[], FrameDescription, string>>();
            BodyIndexQueue = new Queue<Tuple<byte[], FrameDescription, string>>();

            InitializeComponent();
        }

        /// <summary>
        ///     Describes the KinectSensor the current program is connected to
        /// </summary>
        private KinectSensor DefaultKinectSensor { get; set; }

        /// <summary>
        ///     The object that reads frames from the Kinect 2.0 Service. It can read any multitude
        ///     of frame types, including color, depth, infrared, body index, body joints, and audio.
        ///     However, this program does not bother to capture audio or body joint segments.
        /// </summary>
        private MultiSourceFrameReader FrameReader { get; set; }

        /// <summary>
        ///     Member to store the last frame number used across each event
        /// </summary>
        private uint FrameNumber { get; set; }

        /// <summary>
        ///     Defines whether the SaveLatestFrame event handler should run
        /// </summary>
        private bool IsSaveSet { get; set; }

        /// <summary>
        ///     Producer / Consumer Queue to manage write tasks
        /// </summary>
        private ProdConsQueue PcQueue { get; }

        private Queue<Tuple<byte[], FrameDescription, string>> ColorQueue { get; }

        private Queue<Tuple<ushort[], string>> DepthQueue { get; }

        private Queue<Tuple<byte[], FrameDescription, string>> InfraredQueue { get; }

        private Queue<Tuple<byte[], FrameDescription, string>> BodyIndexQueue { get; }

        /// <summary>
        ///     Defines a "destructor"-like function which does cleanup as the window is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowClosing(object sender, CancelEventArgs e)
        {
            if (FrameReader != null)
            {
                FrameReader.Dispose();
                FrameReader = null;
            }

            if (DefaultKinectSensor != null)
            {
                DefaultKinectSensor.Close();
                DefaultKinectSensor = null;
            }

            PcQueue.Shutdown(false);
        }

        /// <summary>
        ///     Defines what happens once the window is loaded. Basically all it does is add the
        ///     ReaderFrameArrived method as a callback to the FrameReader object so it knows to call
        ///     that method once a new frame arrives.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            if (FrameReader == null) return;
            FrameReader.MultiSourceFrameArrived += DisplayLatestFrame;
            FrameReader.MultiSourceFrameArrived += SaveLatestFrame;
            FrameReader.MultiSourceFrameArrived += WriteFrameQueuesToDisk;
        }

        /// <summary>
        ///     Event method which updates the KinectImageDisplay source variable to the latest image
        ///     upon receiving the latest frame information from the Kinect 2.0 Sensor. This is
        ///     automatically hooked into the MultiSourceFrameArrived dispatcher in the WindowLoaded
        ///     method when the MainWindow class loads at program start.
        /// </summary>
        /// <param name="sender">The object which sent the message</param>
        /// <param name="e">The event containing the newly-arrived frame</param>
        private void DisplayLatestFrame(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            var currentFrame = e.FrameReference.AcquireFrame();

            // Check which image type to display, then show it if it exists (not null)
            // NOTE: this.KinectImageDisplay is defined in the XAML file, as is
            // this.VisualizationChoice. I hate to use the explicit cast but it saves me
            // from duplicating code and is easier to read
            switch ((ImageTypes) VisualizationChoice.SelectedIndex)
            {
                case ImageTypes.Color:
                    using (var frame = currentFrame.ColorFrameReference.AcquireFrame())
                        if (frame != null)
                            KinectImageDisplay.Source = FrameDataConverter.FrameToBitmap(frame);
                    break;
                case ImageTypes.Depth:
                    using (var frame = currentFrame.DepthFrameReference.AcquireFrame())
                        if (frame != null)
                            KinectImageDisplay.Source = FrameDataConverter.FrameToBitmap(frame);
                    break;
                case ImageTypes.Infrared:
                    using (var frame = currentFrame.InfraredFrameReference.AcquireFrame())
                        if (frame != null)
                            KinectImageDisplay.Source = FrameDataConverter.FrameToBitmap(frame);
                    break;
                case ImageTypes.Bodyindex:
                    using (var frame = currentFrame.BodyIndexFrameReference.AcquireFrame())
                        if (frame != null)
                            KinectImageDisplay.Source = FrameDataConverter.FrameToBitmap(frame);
                    break;
            }
        }

        /// <summary>
        ///     Performs cleanup / UI enabling after files are saved
        /// </summary>
        private void CleanupAfterSave()
        {
            // More or less the opposite of OnClickSaveFrames
            FrameNumber = 0;
            SaveFrames.IsEnabled = true;
            SaveFilename.IsEnabled = true;
            MaxFramesToSave.IsEnabled = true;
            SaveColorCheck.IsEnabled = true;
            SaveDepthCheck.IsEnabled = true;
            SaveInfraredCheck.IsEnabled = true;
            SaveBodyIndexCheck.IsEnabled = true;
            IsSaveSet = false;
        }

        /// <summary>
        ///     Event handler to save the latest frame if save mode is set.
        /// </summary>
        /// <param name="sender">The object which sent the event</param>
        /// <param name="e">The event (i.e. multi-source frame arrives)</param>
        private void SaveLatestFrame(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            if (IsSaveSet)
            {
                // Increment frame number, store a local copy
                var currentFrame = e.FrameReference.AcquireFrame();
                var currentFrameNumber = ++FrameNumber;
                var rtime = (uint) currentFrame.DepthFrameReference.RelativeTime.TotalMilliseconds;

                // Pathname determination -> each frame starts with this text
                var path = Path.Combine(SaveDirectory.Text, SaveFilename.Text);

                if (currentFrameNumber > Convert.ToUInt32(MaxFramesToSave.Text))
                {
                    CleanupAfterSave();
                    return;
                }

                // Null-coalescing operator ?? => false if IsChecked is null.
                using (var frame = currentFrame.DepthFrameReference.AcquireFrame())
                {
                    if ((SaveDepthCheck.IsChecked ?? false) && frame != null)
                    {
                        lock (_dlock)
                        {
                            // Depth is selected
                            var depthFrameData = FrameDataConverter.FrameToUshortArray(frame);
                            DepthQueue.Enqueue(Tuple.Create(
                                depthFrameData,
                                $"{path}_depth_{rtime}_{currentFrameNumber}.bin"));
                        }
                    }
                }
                using (var frame = currentFrame.ColorFrameReference.AcquireFrame())
                {
                    if ((SaveColorCheck.IsChecked ?? false) && frame != null)
                    {
                        lock (_clock)
                        {
                            // Color is selected
                            var colorFrameData = FrameDataConverter.FrameToByteArray(frame);
                            var colorFrameDesc = frame.CreateFrameDescription(ColorImageFormat.Bgra);
                            ColorQueue.Enqueue(Tuple.Create(
                                colorFrameData,
                                colorFrameDesc,
                                $"{path}_colour_{rtime}_{currentFrameNumber}.png"));
                        }
                    }
                }
                using (var frame = currentFrame.InfraredFrameReference.AcquireFrame())
                {
                    if ((SaveInfraredCheck.IsChecked ?? false) && frame != null)
                    {
                        lock (_ilock)
                        {
                            // Infrared is selected
                            var infraredFrameData = FrameDataConverter.FrameToByteArray(frame);
                            var infraredFrameDesc = frame.FrameDescription;
                            InfraredQueue.Enqueue(Tuple.Create(
                                infraredFrameData,
                                infraredFrameDesc,
                                $"{path}_infrared_{rtime}_{currentFrameNumber}.png"));
                        }
                    }
                }
                using (var frame = currentFrame.BodyIndexFrameReference.AcquireFrame())
                {
                    if ((SaveBodyIndexCheck.IsChecked ?? false) && frame != null)
                    {
                        lock (_block)
                        {
                            // Body index is selected
                            var bodyIndexFrameData = FrameDataConverter.FrameToByteArray(frame);
                            var bodyIndexFrameDesc = frame.FrameDescription;
                            BodyIndexQueue.Enqueue(Tuple.Create(
                                bodyIndexFrameData,
                                bodyIndexFrameDesc,
                                $"{path}_bodyindex_{rtime}_{currentFrameNumber}.png"));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sends data in each of the frame queues to a worker queue which writes the frames to disk.
        /// Only operates if the program is not collecting frames.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WriteFrameQueuesToDisk(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            if (IsSaveSet) return;
            while (ColorQueue.Count > 0)
            {
                var q = ColorQueue.Dequeue();
                PcQueue.EnqueueAction(() =>
                    FrameDataWriter.WriteColorFrameDataToFile(q.Item1, q.Item2, q.Item3));
            }
            while (DepthQueue.Count > 0)
            {
                var q = DepthQueue.Dequeue();
                PcQueue.EnqueueAction(() =>
                    FrameDataWriter.WriteDepthFrameDataToFile(q.Item1, q.Item2));
            }
            while (InfraredQueue.Count > 0)
            {
                var q = InfraredQueue.Dequeue();
                PcQueue.EnqueueAction(() =>
                    FrameDataWriter.WriteInfraredFrameDataToFile(q.Item1, q.Item2, q.Item3));
            }
            while (BodyIndexQueue.Count > 0)
            {
                var q = BodyIndexQueue.Dequeue();
                PcQueue.EnqueueAction(() =>
                    FrameDataWriter.WriteBodyIndexFrameDataToFile(q.Item1, q.Item2, q.Item3));
            }
        }

        /// <summary>
        ///     Opens a folder dialog to locate the folder you want to save to
        /// </summary>
        /// <param name="sender">Object that sent the click event</param>
        /// <param name="e">The click event</param>
        private void OnClickBrowseDirectory(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.ShowDialog();
            if (dialog.SelectedPath != "")
            {
                SaveDirectory.Text = dialog.SelectedPath;
            }
        }

        /// <summary>
        ///     Handles events that interact with the SaveFrames button object
        /// </summary>
        /// <param name="sender">The object which sent the event</param>
        /// <param name="e">The event (e.g. click on the button)</param>
        private void OnClickSaveFrames(object sender, RoutedEventArgs e)
        {
            // Disables fields needed in order to save frames
            SaveFrames.IsEnabled = false;
            SaveFilename.IsEnabled = false;
            MaxFramesToSave.IsEnabled = false;
            SaveColorCheck.IsEnabled = false;
            SaveDepthCheck.IsEnabled = false;
            SaveInfraredCheck.IsEnabled = false;
            SaveBodyIndexCheck.IsEnabled = false;
            IsSaveSet = true;
        }

        /// <summary>
        ///     Enum type to differentiate between different frame types in the UI
        /// </summary>
        private enum ImageTypes
        {
            Color,
            Depth,
            Infrared,
            Bodyindex
        };
    }
}
