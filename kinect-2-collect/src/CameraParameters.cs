﻿/*
 * Copyright (c) 2015, Jeremy Steward
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;

namespace Kinect2_Collect
{
    /// <summary>
    ///     Defines a class which contains the camera parameters for a camera
    /// </summary>
    internal class CameraParameters
    {
        /// <summary>
        ///     Constructor for initializing the class parameters
        /// </summary>
        /// <param name="width">Width of image extent (in pixels)</param>
        /// <param name="height">Height of image extent (in pixels)</param>
        /// <param name="_f0">
        ///     Uncorrected (nominal) focal length of camera (put 1 or same as f if unsure)
        /// </param>
        /// <param name="_f">Focal length of camera (principal distance, in pixels or mm)</param>
        /// <param name="_xp">Principal point offset in x (in pixels or mm)</param>
        /// <param name="_yp">Principal point offset in y (in pixels or mm)</param>
        /// <param name="_k1">First order radial lens distortion coefficient</param>
        /// <param name="_k2">Second order radial lens distortion coefficient</param>
        /// <param name="_k3">Third order radial lens distortion coefficient</param>
        /// <param name="_p1">First decentering lens distortion coefficient</param>
        /// <param name="_p2">Second decentering lens distortion coefficient</param>
        /// <param name="_d0">Rangefinder offset (in mm)</param>
        public CameraParameters(
            uint width, uint height, double _f0,
            double _f, double _xp, double _yp,
            double _k1, double _k2, double _k3,
            double _p1, double _p2, double _d0)
        {
            Width = width;
            Height = height;
            f0 = _f0;
            f = _f;
            xp = _xp;
            yp = _yp;
            K1 = _k1;
            K2 = _k2;
            K3 = _k3;
            P1 = _p1;
            P2 = _p2;
            D0 = _d0;
        }

        /// <summary>
        ///     Width of the image
        /// </summary>
        public uint Width { get; }

        /// <summary>
        ///     Height of the image
        /// </summary>
        public uint Height { get; }

        /// <summary>
        ///     Focal length of camera
        /// </summary>
        public double f { get; }

        /// <summary>
        ///     Uncorrected (or nominal) focal length of the camera
        /// </summary>
        public double f0 { get; }

        /// <summary>
        ///     Principal distance in x
        /// </summary>
        public double xp { get; }

        /// <summary>
        ///     Principal distance in y
        /// </summary>
        public double yp { get; }

        /// <summary>
        ///     First radial lens distortion coefficient
        /// </summary>
        public double K1 { get; }

        /// <summary>
        ///     Second radial lens distortion coefficient
        /// </summary>
        public double K2 { get; }

        /// <summary>
        ///     Third radial lens distortion coefficient
        /// </summary>
        public double K3 { get; }

        /// <summary>
        ///     First decentering lens distortion coefficient
        /// </summary>
        public double P1 { get; }

        /// <summary>
        ///     Second decentering lens distortion coefficient
        /// </summary>
        public double P2 { get; }

        /// <summary>
        ///     Rangefinder offset
        /// </summary>
        public double D0 { get; }
    }

    internal static class CameraCorrections
    {
        public static IEnumerable<Tuple<double, double, double>> XyzFromDepthFrame(ushort[] frame, CameraParameters cparams)
        {
            // Final output array
            var xyz = new List<Tuple<double, double, double>>();

            // i represents pixel row
            for (uint i = 0; i < cparams.Height; ++i)
            {
                // j represents pixel column
                for (uint j = 0; j < cparams.Width; ++j)
                {
                    var x = j - ((((double)cparams.Width) / 2) - 0.5);
                    var y = ((((double)cparams.Height) / 2) - 0.5) - i;

                    // Divided by 1000 since the numbers are in mm and metres is preferred
                    var Z = (double) (frame[j + (i*cparams.Width)])/1000;
                    // Check if any measurement is less than zero, if so, set it to zero
                    Z = Z < 0 ? 0 : Z;

                    var X = -(Z/cparams.f0)*x;
                    var Y = (Z/cparams.f0)*y;
                    var rho = Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2) + Math.Pow(Z, 2));

                    var xbar = x - cparams.xp; // x, y, radial term with principal point offsets added
                    var ybar = y - cparams.yp; // x, y, radial term with principal point offsets added
                    var r = Math.Sqrt(Math.Pow(xbar, 2) + Math.Pow(ybar, 2)); // x, y, radial term with principal point offsets added

                    var radx = xbar*(cparams.K1*Math.Pow(r, 2) + cparams.K2*Math.Pow(r, 4)); // Radial lens corrections
                    var rady = ybar*(cparams.K1*Math.Pow(r, 2) + cparams.K2*Math.Pow(r, 4)); // Radial lens corrections

                    var decx = cparams.P1*(Math.Pow(ybar, 2) + 3*Math.Pow(xbar, 2)) + 2*cparams.P2*xbar*ybar; // Decentering lens corrections
                    var decy = cparams.P2*(Math.Pow(xbar, 2) + 3*Math.Pow(ybar, 2)) + 2*cparams.P1*xbar*ybar; // Decentering lens corrections

                    // Final corrected image coordinates
                    x = xbar - radx - decx;
                    y = ybar - rady - decy;
                    rho = rho - cparams.D0;

                    var lambda = rho/Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(cparams.f, 2));
                    xyz.Add(Tuple.Create(
                        lambda*x,
                        lambda*y,
                        lambda*cparams.f));
                }
            }
            return xyz;
        }
    }
}
