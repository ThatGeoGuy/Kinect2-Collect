﻿/*
 * Copyright (c) 2015, Jeremy Steward
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Threading;

namespace Kinect2_Collect
{
    /// <summary>
    ///     Specifies a producer / consumer queue that uses a set amount of worker threads.
    /// </summary>
    internal class ProdConsQueue
    {
        /// <summary>
        ///     Object to lock so there's no thread contention for the queue
        /// </summary>
        private readonly object _locker = new object();

        /// <summary>
        ///     Constructs a Producer / Consumer Queue object.
        /// </summary>
        /// <param name="workerCount">Number of worker threads to use.</param>
        public ProdConsQueue(uint workerCount)
        {
            Workers = new Thread[workerCount];
            JobQueue = new Queue<Action>();

            // Create and start a separate thread for each worker
            for (var i = 0; i < workerCount; ++i)
            {
                Workers[i] = new Thread(Consume);
                Workers[i].Start();
            }
        }

        /// <summary>
        ///     Array of all worker threads
        /// </summary>
        private Thread[] Workers { get; }

        /// <summary>
        ///     The queue of jobs to complete. These are represented as zero arity delegates (actions)
        /// </summary>
        private Queue<Action> JobQueue { get; }

        /// <summary>
        ///     Shuts down the Producer / Consumer Queue by joining all the worker threads
        /// </summary>
        /// <param name="waitForWorkers"></param>
        public void Shutdown(bool waitForWorkers)
        {
            // Enqueue a null item per each worker
            foreach (var w in Workers)
            {
                EnqueueAction(null);
            }

            // Wait for workers to finish
            if (!waitForWorkers) return;
            foreach (var w in Workers)
            {
                w.Join();
            }
        }

        /// <summary>
        ///     Enqueues an Action delegate into the job queue, after safely acquiring lock.
        /// </summary>
        /// <param name="item">The zero arity delegate to insert into the job queue</param>
        public void EnqueueAction(Action item)
        {
            lock (_locker)
            {
                JobQueue.Enqueue(item);
                Monitor.Pulse(_locker);
            }
        }

        /// <summary>
        ///     Consumes a job that has been enqueued
        /// </summary>
        private void Consume()
        {
            // Keep consuming until a null job is found
            while (true)
            {
                Action job;
                lock (_locker)
                {
                    while (JobQueue.Count == 0)
                    {
                        Monitor.Wait(_locker);
                    }
                    job = JobQueue.Dequeue();
                }
                if (job == null)
                    return;
                job();
            }
        }
    }
}
