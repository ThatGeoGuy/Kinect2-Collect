﻿/*
 * Copyright (c) 2015, Jeremy Steward
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;

namespace Kinect2_Collect
{
    internal static class FrameDataConverter
    {
        /// <summary>
        ///     Converts a ColorFrame object from the Kinect 2.0 Sensor into a byte array.
        /// </summary>
        /// <param name="frame">The ColorFrame object received from the Kinect Sensor</param>
        /// <returns>
        ///     A byte array containing the pixel information from the ColorFrame in BGRA format
        /// </returns>
        public static byte[] FrameToByteArray(ColorFrame frame)
        {
            var frameDesc = frame.CreateFrameDescription(ColorImageFormat.Bgra);
            var bufferLength = frameDesc.LengthInPixels*frameDesc.BytesPerPixel;
            var pixels = new byte[bufferLength];
            frame.CopyConvertedFrameDataToArray(pixels, ColorImageFormat.Bgra);
            return pixels;
        }

        /// <summary>
        ///     Converts a DepthFrame object from the Kinect 2.0 Sensor into a byte array.
        /// </summary>
        /// <param name="frame">The DepthFrame object received from the Kinect Sensor</param>
        /// <returns>
        ///     A byte array containing the pixel information from the DepthFrame in BGRA format
        /// </returns>
        public static byte[] FrameToByteArray(DepthFrame frame)
        {
            var frameDesc = frame.FrameDescription;
            var depths = FrameToUshortArray(frame);

            // Kinect 2 Docs say that depth is 2 bytes per pixel, however, 
            // here I want 4, so it displays better when creating a bitmap
            var bufferLength = (uint) (depths.Length*2*frameDesc.BytesPerPixel);

            var pixelArray = new byte[bufferLength];

            for (var i = 0; i < depths.Length; ++i)
            {
                var currentDepth = depths[i];
                var depthIntensity = (byte) currentDepth;

                pixelArray[(4*i) + 0] = depthIntensity; // Blue
                pixelArray[(4*i) + 1] = depthIntensity; // Green
                pixelArray[(4*i) + 2] = depthIntensity; // Red
                pixelArray[(4*i) + 3] = 255; // Alpha
            }
            return pixelArray;
        }

        /// <summary>
        ///     Converts an InfraredFrame object from the Kinect 2.0 Sensor into a byte array.
        /// </summary>
        /// <param name="frame">The InfraredFrame object received from the Kinect Sensor</param>
        /// <returns>
        ///     A byte array containing the pixel information from the InfraredFrame in Gray16 format
        /// </returns>
        public static byte[] FrameToByteArray(InfraredFrame frame)
        {
            var frameDesc = frame.FrameDescription;
            var bufferLength = frameDesc.LengthInPixels*frameDesc.BytesPerPixel;
            var amplitudes = FrameToUshortArray(frame);
            var pixels = new byte[bufferLength];

            for (var i = 0; i < amplitudes.Length; ++i)
            {
                var currentIntensity = BitConverter.GetBytes(amplitudes[i]);
                pixels[(2*i) + 0] = currentIntensity[0];
                pixels[(2*i) + 1] = currentIntensity[1];
            }
            return pixels;
        }

        /// <summary>
        ///     Converts a BodyIndexFrame object from the Kinect 2.0 Sensor into a byte array.
        /// </summary>
        /// <param name="frame">The BodyIndexFrame object received from the Kinect Sensor</param>
        /// <returns></returns>
        public static byte[] FrameToByteArray(BodyIndexFrame frame)
        {
            var frameDesc = frame.FrameDescription;
            var bufferLength = frameDesc.LengthInPixels*frameDesc.BytesPerPixel;
            var bodyIndexAmplitudes = new byte[bufferLength];
            frame.CopyFrameDataToArray(bodyIndexAmplitudes);
            return bodyIndexAmplitudes;
        }

        /// <summary>
        ///     Converts a DepthFrame object from the Kinect 2.0 Sensor into a ushort array.
        /// </summary>
        /// <param name="frame">
        ///     The DepthFrame object received from the Kinect Sensor
        /// </param>
        /// <returns>
        ///     A ushort array containing the depth information at each pixel in row-major order
        /// </returns>
        public static ushort[] FrameToUshortArray(DepthFrame frame)
        {
            var frameDesc = frame.FrameDescription;
            var bufferLength = frameDesc.LengthInPixels;
            var depths = new ushort[bufferLength];
            frame.CopyFrameDataToArray(depths);
            return depths;
        }

        /// <summary>
        ///     Converts an InfraredFrame object from the Kinect 2.0 Sensor into a ushort array.
        /// </summary>
        /// <param name="frame">
        ///     The InfraredFrame object received from the Kinect Sensor
        /// </param>
        /// <returns>
        ///     A ushort array containing the amplitude information at each pixel in row-major order
        /// </returns>
        public static ushort[] FrameToUshortArray(InfraredFrame frame)
        {
            var frameDesc = frame.FrameDescription;
            var bufferLength = frameDesc.LengthInPixels;
            var amplitudes = new ushort[bufferLength];
            frame.CopyFrameDataToArray(amplitudes);
            return amplitudes;
        }

        /// <summary>
        ///     Converts a ColorFrame object from the Kinect 2.0 sensor into a BitmapSource.
        /// </summary>
        /// <param name="frame">The ColorFrame object received from the Kinect Sensor</param>
        /// <returns>
        ///     A BitmapSource object containing one frame of data from the Kinect 2.0 color camera
        /// </returns>
        public static ImageSource FrameToBitmap(ColorFrame frame)
        {
            var frameDesc = frame.CreateFrameDescription(ColorImageFormat.Bgra);
            var width = frameDesc.Width;
            var height = frameDesc.Height;
            var stride = width*((int) frameDesc.BytesPerPixel);
            var pixels = FrameToByteArray(frame);
            return BitmapSource.Create(width, height, 96, 96, PixelFormats.Bgra32, null, pixels, stride);
        }

        /// <summary>
        ///     Converts a DepthFrame object from the Kinect 2.0 sensor into a BitmapSource.
        /// </summary>
        /// <param name="frame">The DepthFrame object received from the Kinect Sensor</param>
        /// <returns>
        ///     A BitmapSource object containing one frame of data from the Kinect 2.0 range camera (depth)
        /// </returns>
        public static ImageSource FrameToBitmap(DepthFrame frame)
        {
            var frameDesc = frame.FrameDescription;
            var width = frameDesc.Width;
            var height = frameDesc.Height;

            // As was the case in FrameToByteArray, depth frames are 2 bytes per pixel,
            // but I want 4 so that I can display it in a nicer fashion
            var stride = width*2*((int) frameDesc.BytesPerPixel);
            var pixels = FrameToByteArray(frame);
            return BitmapSource.Create(width, height, 96, 96, PixelFormats.Bgra32, null, pixels, stride);
        }

        /// <summary>
        ///     Converts an InfraredFrame object from the Kinect 2.0 sensor into a BitmapSource.
        /// </summary>
        /// <param name="frame">The InfraredFrame object received from the Kinect Sensor</param>
        /// <returns>
        ///     A BitmapSource object containing one frame of data from the Kinect 2.0 range camera (amplitude)
        /// </returns>
        public static ImageSource FrameToBitmap(InfraredFrame frame)
        {
            var frameDesc = frame.FrameDescription;
            var width = frameDesc.Width;
            var height = frameDesc.Height;
            var stride = width*((int) frameDesc.BytesPerPixel);
            var pixels = FrameToByteArray(frame);
            return BitmapSource.Create(width, height, 96, 96, PixelFormats.Gray16, null, pixels, stride);
        }

        /// <summary>
        ///     Converts a BodyIndexFrame object from the Kinect 2.0 sensor into a BitmapSource.
        /// </summary>
        /// <param name="frame">The BodyIndexFrame object received from the Kinect Sensor</param>
        /// <returns>
        ///     A BitmapSource object containing one frame of data from the Kinect 2.0 range camera (body index)
        /// </returns>
        public static ImageSource FrameToBitmap(BodyIndexFrame frame)
        {
            var frameDesc = frame.FrameDescription;
            var width = frameDesc.Width;
            var height = frameDesc.Height;
            var stride = width*((int) frameDesc.BytesPerPixel);
            var pixels = FrameToByteArray(frame);
            return BitmapSource.Create(width, height, 96, 96, PixelFormats.Gray8, null, pixels, stride);
        }
    }
}
