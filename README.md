# Kinect 2 Collect

Kinect 2 Collect (K2C) is a program written in C# designed to collect arbitrary frame data from the multitude of sensors on board the Microsoft Kinect 2.0 device. It aims to provide a clean, coherent way of interacting with the various sensors and data types on board the device, specifically aiming to provide accurate measurements using the Kinect 2.0 Time-of-flight camera sensor.

This project is still a work in progress, and still has a long way to go before it is a full-fledged solution.

## License

The entirety of the code provided here is provided under the new BSD (3 clause) license, except those parts of the project that belong to other parties (e.g. the Kinect 2.0 driver provided by the Kinect for Windows SDK by Microsoft). See LICENSE for more details.
